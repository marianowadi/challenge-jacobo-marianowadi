# Front-end test - Mariano Wadi Jacobo
---
### AirBnb-like frontend app
#### Instrucciones:
1. Clonar el repo 👉 `git clone git@gitlab.com:marianowadi/challenge-jacobo-marianowadi.git`
2. Entrar a la carpeta 👉 `cd challenge-jacobo-marianowadi`
3. Instalar las dependencias 👉 `npm i`
4. Invocar la magia Angular 👉 `ng serve -o`
5. Voilá! 🚀
