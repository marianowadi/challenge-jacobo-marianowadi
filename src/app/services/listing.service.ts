import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Listing } from '../model/Listing';

@Injectable({
  providedIn: 'root'
})
export class ListingService {

  constructor(
    private http: HttpClient
  ) { }

  /**
 * Get listing information from API.
 *
 * @return {Listing}
 */

  getListing() {
    return this.http.get<Listing>(`${environment.apiUrl}/listing-information.json`);
  }
}
