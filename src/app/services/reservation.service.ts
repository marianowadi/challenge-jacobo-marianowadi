import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Cost } from '../model/Cost';
import { Reservation } from '../model/Reservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(
    private http: HttpClient
  ) { }

  
  private reservation = new BehaviorSubject<Reservation>({
    checkin: undefined,
    checkout: undefined,
    adults: 1,
    children: 0,
    pets: false,
    cost: undefined,
    message: undefined
  });

  

  currentReservation = this.reservation.asObservable();
  

/**
 * Calculates reservation cost based on reservation data.
 * @param {Reservation} 
 * @return {Cost}
 */
  calculateCost(reservation: Reservation): Cost {
    let apiData = new Cost;
    const checkinDate = new Date(reservation.checkin);
    const checkoutDate = new Date(reservation.checkout);
    const diffNights = (checkoutDate.getTime() - checkinDate.getTime()) / (1000 * 3600 * 24);
    apiData.cleaning_fee = 15.68;
    apiData.discount = 33;
    apiData.nights_cost = 95.82;
    apiData.nights_count = diffNights;
    apiData.total = Math.round(((apiData.nights_cost * apiData.nights_count) - apiData.discount + apiData.cleaning_fee) * 100 ) /100;
    return apiData;
  }

/**
 * Posts reservation to API.
 * @param {Reservation} 
 * @return {String}
 */
  confirmReservation(reservation: Reservation) {
    return this.http.post("api/listings/1/confirm-reservation", reservation);
  }

/**
 * Updates reservation BehavioSubject based on current provided Reservation param.
 * @param {Reservation} 
 * @return {BehaviorSubject}
 */
  updateReservation(reservation: Reservation) {
    this.reservation.next(reservation);
  }

  

}
