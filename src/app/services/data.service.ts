import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private reviewing = new BehaviorSubject<boolean>(true);
  isReviewing = this.reviewing.asObservable();

/**
 * Updates reviewing state based on latest value.
 * @param {Boolean} 
 * @return {BehaviorSubject}
 */
  updateState(value: boolean) {
    this.reviewing.next(value);
  }
}
