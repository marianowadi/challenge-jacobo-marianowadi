import { Cost } from './Cost';

export class Reservation {
    checkin: Date;
    checkout: Date;
    adults: number;
    children: number;
    pets: boolean;
    cost: Cost;
    message: string;
}