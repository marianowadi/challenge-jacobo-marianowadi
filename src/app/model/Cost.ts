export class Cost {
    nights_count: number;
    nights_cost: number;
    discount: number;
    cleaning_fee: number;
    total: number;
}