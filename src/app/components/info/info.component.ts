import { Component, OnInit } from '@angular/core';
import { Cost } from 'src/app/model/Cost';
import { Listing } from 'src/app/model/Listing';
import { Reservation } from 'src/app/model/Reservation';
import { DataService } from 'src/app/services/data.service';
import { ListingService } from 'src/app/services/listing.service';
import { ReservationService } from 'src/app/services/reservation.service';

@Component({
  selector: 'reservation-information',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {
  listing: Listing;
  reservation: Reservation;
  isReview: boolean;
  successText: string;

  constructor(
    private listingService: ListingService,
    private reservationService: ReservationService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.dataService.isReviewing.subscribe(value => this.isReview = value);
    this.reservationService.currentReservation.subscribe(reservation => this.reservation = reservation);
    this.listingService.getListing().subscribe(listing => this.listing = listing);
  }

/**
 * Sets reservation reviewing state to `false` if reservation cost has been calculated.
 */
  confirm() {
    if (this.reservation.cost) {
      this.dataService.updateState(false);
    }
  }

/**
 * Posts reservation to API.
 * @return {String}
 */
  confirmReservation() {
    this.reservationService
    .confirmReservation(this.reservation)
    .subscribe(response => this.successText = response['message']);
  }

}
