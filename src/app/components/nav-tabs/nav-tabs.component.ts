import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'nav-tabs',
  templateUrl: './nav-tabs.component.html',
  styleUrls: ['./nav-tabs.component.css']
})
export class NavTabsComponent implements OnInit {
  isReview: boolean;

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.dataService.isReviewing.subscribe(value => this.isReview = value);
  }

/**
 * Sets reservation reviewing state back to `true`.
 */
  navReview() {
    this.isReview = true;
    this.dataService.updateState(true);
  }

}
