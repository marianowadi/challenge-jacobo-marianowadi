import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Cost } from 'src/app/model/Cost';
import { Listing } from 'src/app/model/Listing';
import { Reservation } from 'src/app/model/Reservation';
import { DataService } from 'src/app/services/data.service';
import { ListingService } from 'src/app/services/listing.service';
import { ReservationService } from 'src/app/services/reservation.service';

@Component({
  selector: 'reservation-cost',
  templateUrl: './reservation-cost.component.html',
  styleUrls: ['./reservation-cost.component.css']
})
export class ReservationCostComponent implements OnInit {
  reservation: Reservation;
  listing: Listing;
  reservationForm: FormGroup;
  reservationCost: Cost;
  isReview: boolean;

  constructor(
    private reservationService: ReservationService,
    private dataService: DataService,
    private listingService: ListingService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.reservationService.currentReservation.subscribe(reservation => this.reservation = reservation);
    this.dataService.isReviewing.subscribe(value => this.isReview = value);
    this.listingService.getListing().subscribe(listing => this.listing = listing);
    this.buildForm(this.reservation);
    this.listen();
  }

/**
 * Builds reservation form from API data.
 *
 * @param {Reservation}
 */
  buildForm(data: Reservation): void {
    this.reservationForm = this.formBuilder.group({
      checkin: [data.checkin, Validators.required],
      checkout: [data.checkout, Validators.required],
      adults: [data.adults, Validators.required],
      children: data.children,
      pets: data.pets
    })
  }

  get adults() {
    return this.reservationForm.get('adults');
  }

  get children() {
    return this.reservationForm.get('children');
  }

  get checkin() {
    return this.reservationForm.get('checkin').value;
  }

  get checkout() {
    return this.reservationForm.get('checkout').value;
  }
/**
 * Increases reservation adults count by 1 unless listing adults limit has been reached.
 */
  increaseAdults(): void {
    if (this.adults.value <= this.listing.adults) {
      this.adults.patchValue(this.adults.value + 1);
    }
  }
/**
 * Decreases reservation adults count by 1 to at most 1 adult.
 */
  decreaseAdults(): void {
    if (this.adults.value > 1) {
      this.adults.patchValue(this.adults.value - 1);
    }
  }
/**
 * Increases reservation children count by 1 unless listing children limit has been reached.
 */
  increaseChildren(): void {
    if (this.children.value <= this.listing.children) {
      this.children.patchValue(this.children.value + 1);
    }
  }
/**
 * Decreases reservation children count by 1
 */
  decreaseChildren(): void {
    this.children.patchValue(this.children.value - 1);
  }

/**
 * 1.Calculates reservation cost.
 * 2. Assigns reservation cost return value to reservation cost property.
 * 3. Emits latest Reservation value to BehaviorSubject.
 */
  calculateReservation() {
    this.reservationCost = this.reservationService.calculateCost(this.reservationForm.value);
    this.reservation.cost = this.reservationCost;
    this.updateReservation(this.reservation);
  }

/**
 * Listens to changes on Reservation form.
 * 1. Emits latest Reservation value to BehaviorSubject.
 * 2. Calculates Reservation cost if all fields filled in.
 */
  listen(): void {
    this.reservationForm.valueChanges.subscribe(value => {
      this.reservationService.updateReservation(value);
      if (this.reservationForm.valid) {
        this.calculateReservation();
      }
    })
  }

/**
 * Emits latest value to Reservation BehaviorSubject.
 *
 * @param {Reservation}
 */
  updateReservation(value: Reservation) {
    this.reservationService.updateReservation(value);
  }

}
