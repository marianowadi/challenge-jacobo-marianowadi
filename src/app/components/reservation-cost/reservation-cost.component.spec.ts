import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationCostComponent } from './reservation-cost.component';

describe('ReservationCostComponent', () => {
  let component: ReservationCostComponent;
  let fixture: ComponentFixture<ReservationCostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationCostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
