import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'

import { NavTabsComponent } from './components/nav-tabs/nav-tabs.component';
import { InfoComponent } from './components/info/info.component';
import { ReservationCostComponent } from './components/reservation-cost/reservation-cost.component';
import { BooleanPipe } from './utils/boolean.pipe';


@NgModule({
  declarations: [
    AppComponent,
    NavTabsComponent,
    InfoComponent,
    ReservationCostComponent,
    BooleanPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, 
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
