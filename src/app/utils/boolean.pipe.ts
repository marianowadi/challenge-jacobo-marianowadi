import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'boolean'
})
export class BooleanPipe implements PipeTransform {

/**
 * Formats boolean value to 'Yes'/'False'.
 * @param {Boolean}
 * @return {String}
 */
  transform(value: boolean): string {
    return value === true ? 'Yes' : 'No';
  }

}
